export const data = [
  {
    inviteName: 'Jared and Natasha Smith',
    partySize: 2,
    confirmed: false,
    attendees: ['Jared Smith', 'Natasha Smith'],
    code: '81d8d0'
  },
  {
    inviteName: 'Brian and Barbara Naylor',
    partySize: 2,
    confirmed: false,
    attendees: ['Brian Naylor', 'Barbara Naylor'],
    code: 'b2a992'
  },
  {
    inviteName: 'AJ Kinnee',
    partySize: 1,
    confirmed: false,
    attendees: ['AJ Kinnee'],
    code: '229074'
  }
]