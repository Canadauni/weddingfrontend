import Home from './components/Home.vue';
import Rsvp from './components/Rsvp.vue';
import Confirmed from './components/Confirmed.vue';
import Travel from './components/Travel.vue';
import Registry from './components/Registry.vue';

export const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/RSVP',
    name: 'rsvp',
    component: Rsvp
  },
  {
    path: '/Travel',
    name: 'travel',
    component: Travel
  },
  {
    path: '/Registry',
    name: 'registry',
    component: Registry
  },
  {
    path: '/Confirmed',
    name: 'confirmed',
    component: Confirmed,
    beforeEnter: (to, from, next) => {
      console.log(from);
      if(from.name == null) {
        next('/RSVP');
      } else {
        next();
      }
    }
  }
]
